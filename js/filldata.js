var alumnes = [{
        firstName: "Albert",
        lastName: "Roca",
        age: 20,
        eyeColor: "blue",
        city: "Manresa"
    },
    {
        firstName: "Carles",
        lastName: "Garcia",
        age: 17,
        eyeColor: "brown",
        city: "Sant Vicenç de Castellet"
    },
    {
        firstName: "Mònica",
        lastName: "Altarriba",
        age: 18,
        eyeColor: "brown",
        city: "Santpedor"
    },
               
];
/* Carrega la informació a les pàgines just despreś d'haver carregat tots els components*/
// La plantilla_alumnes està definida en el fitxer templates.js

$(function(){
   $("#alumnes").html(Mustache.render(plantilla_alumnes,alumnes));
});



